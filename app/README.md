# treeShareApp

Share your tree with the community

<img src="https://media.istockphoto.com/vectors/tree-network-digital-vector-id679659086?k=20&m=679659086&s=612x612&w=0&h=z86OoQugSi1AhZ6SUzLVUNxnRXvsH0Grn5jF1-wLn-Y="/>

## Getting Started

Resources and Docs to getting started:
- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
- [online documentation](https://flutter.dev/docs)

### Getting started with this project in IntelliJ:

- Install Flutter [here](https://docs.flutter.dev/get-started/install)
- Install the Flutter Plugin for IntelliJ: under ```File > Settings > Plugins``` and search for 'Flutter'
- Open this Project in IntelliJ (IntelliJ should detect that this is a Flutter App Project)
- Open your Terminal in IntelliJ and type the following command: ```flutter doctor```
- It should show the following message:
```
len-6140@nnvf3361:~/DHBW/SE1/git/treeShare/app$ flutter doctor
Doctor summary (to see all details, run flutter doctor -v):
[✓] Flutter (Channel stable, 2.5.3, on Ubuntu 20.04.3 LTS 5.11.0-34-generic, locale de_DE.UTF-8)
[✓] Android toolchain - develop for Android devices (Android SDK version 31.0.0)
[✓] Chrome - develop for the web
[✓] Android Studio (version 2020.3)
[✓] IntelliJ IDEA Ultimate Edition (version 2021.2)
[✓] VS Code
[✓] Connected device (1 available)

• No issues found!
```
- If something is missed [x] you have to install it
- Connecting flutter to the Android SDK:
```
flutter config --android-sdk <path-to-your-android-sdk-path>
```
# Build the app
Build the app with the command:
```
flutter build web
```
in this directory. This will build a web application bundle.
NOTE: Builds for iOS and Android are also possible but we tested this only for the Webbrowser.

# Run the app
If you want to run the app locally you have to call
```
flutter run -d chrome
```
in your bash/shell. For that you have to install flutter and Google Chrome.
Important NOTE:
This app is for simplicity only in chrome tested.
You have to run the Server (see <ProjectDir>/webservice/README.md) on localhost on the same device as this app.
Otherwise, the app can't reach the server...

# Setup: Android Virtual Device (AVD) in IntelliJ
- Probably first you have to connect IntelliJ with your downloaded Android SDK to unlock this tool. Set your Android SDK in the Android SDK Manager: ```Tools > Android > SDK Manager```
- Open the AVD Manager: ```Tools > Android > AVD Manager```
- Create a new virtual device
- Start your Application with the connected AVD
- Probably you have to adjust the RAM and HEAP size of your AVD, because it's too slow. Adjusting and editing this in the IDE is probably not possible. For this problem do the following steps:
- ![](../docs/imgs/edit_avd_settings.png)
