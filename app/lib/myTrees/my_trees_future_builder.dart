import 'package:flutter/material.dart';
import 'package:tree_share_app/treeListing/tree_list.dart';

import '../ServerRequests/Tree.dart';
import '../ServerRequests/TreeRequests.dart';
import '../core/global_session_props.dart';

class MyTreesFutureBuilder extends StatelessWidget {
  const MyTreesFutureBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getMyTrees(),
      builder: (context, snap) {
        if (snap.data == null) {
          return const Center(child: CircularProgressIndicator(),);
        }
        return TreeList(trees: snap.data as List<Tree>);
      },
    );
  }

  Future<List<Tree>> getMyTrees() async {
    final tr = TreeRequests.withGlobalHost();
    final treeInfos = await tr.search(TreeCriteria(users: [sessionUsername == null ? "" : sessionUsername!]));
    final trees = <Tree> [];
    for (final treeInfo in treeInfos) {
      trees.add(await tr.getById(treeInfo.id!));
    }
    return trees;
  }

}