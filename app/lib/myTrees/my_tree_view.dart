import 'package:flutter/material.dart';
import 'package:tree_share_app/myTrees/add_tree_button.dart';
import 'package:tree_share_app/myTrees/login_required_body.dart';
import 'package:tree_share_app/myTrees/my_trees_future_builder.dart';
import 'package:tree_share_app/widgets.dart';

import '../core/global_session_props.dart';

class MyTreesView extends StatelessWidget {
  const MyTreesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: const NavDrawer(),
        appBar: AppBar(
          title: const Text("My trees"),
        ),
        body: sessionUsername == null
            ? const Center(
                child: const Center(
                    child: Text("Please login to use this function")),
              )
            : const MyTreesFutureBuilder(),
        floatingActionButton: const AddTreeButton());
  }
}
