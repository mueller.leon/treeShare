import 'package:flutter/material.dart';
import 'package:tree_share_app/core/login_required_button.dart';

import '../Config/tree_config_view.dart';

class AddTreeButton extends StatelessWidget {
  const AddTreeButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LoginRequiredButton(
      child: const Icon(Icons.add),
      onPressed: () async {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => const TreeConfigView(addMode: true)));
      },
      tooltip: "Add a tree",
    );
  }
}
