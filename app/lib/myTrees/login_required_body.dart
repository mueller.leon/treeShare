import 'package:flutter/material.dart';

class LoginRequiredBody extends StatelessWidget {
  const LoginRequiredBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Text("Please login to use this function");
  }

}