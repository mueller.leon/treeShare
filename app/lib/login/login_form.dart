import 'package:flutter/material.dart';
import 'package:tree_share_app/login/inherited_login_form.dart';
import 'package:tree_share_app/login/login_button.dart';
import 'package:tree_share_app/login/password_text_form_field.dart';
import 'package:tree_share_app/login/username_text_form_field.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LoginFormState();
  }
}

class _LoginFormState extends State<LoginForm> {

  @override
  Widget build(BuildContext context) {
    return Form(
      key: InheritedLoginForm.of(context).loginFormKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            UsernameTextFormField(),
            SizedBox(height: 15,),
            PasswordTextFormField(),
            SizedBox(height: 20,),
            LoginButton()
          ],
        )
    );
  }
}
