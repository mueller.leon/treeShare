import 'package:flutter/material.dart';
import 'package:tree_share_app/login/inherited_login_form.dart';

class UsernameTextFormField extends StatelessWidget {
  const UsernameTextFormField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: const InputDecoration(
          hintText: "Your unique identifier...",
          label: Text("Username"),
          border: OutlineInputBorder()),
      validator: _validate,
      onSaved: (value) {
        InheritedLoginForm.of(context).loginFormData.username = value;
      },
    );
  }

  String? _validate(String? input) {
    if (input == null || input.isEmpty) {
      return "Username can't be empty";
    }
    return null;
  }
}
