import 'package:flutter/material.dart';
import 'package:tree_share_app/core/global_session_props.dart';
import 'package:tree_share_app/login/inherited_login_form.dart';

class LoginButton extends StatelessWidget {
  const LoginButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        final inheritedParent = InheritedLoginForm.of(context);
        if (inheritedParent.loginFormKey.currentState!.validate()) {
          inheritedParent.loginFormKey.currentState!.save();
          sessionUsername = inheritedParent.loginFormData.username; // TODO
          Navigator.pop(context);
        }
      },
      child: const Text("Login"),
    );
  }
}