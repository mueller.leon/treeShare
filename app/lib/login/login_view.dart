import 'package:flutter/material.dart';
import 'package:tree_share_app/login/inherited_login_form.dart';
import 'package:tree_share_app/widgets.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: Center(
        child: FractionallySizedBox(
          widthFactor: 1 / 3.0,
          child: InheritedLoginForm(),
        ),
      ),
    );
  }

}