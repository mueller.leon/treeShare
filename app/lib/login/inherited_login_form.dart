import 'package:flutter/material.dart';

import 'login_form.dart';
import 'login_form_data.dart';

class InheritedLoginForm extends InheritedWidget {
  final loginFormData = LoginFormData();
  final loginFormKey = GlobalKey<FormState>();

  InheritedLoginForm({Key? key})
      : super(key: key, child: const LoginForm());

  static InheritedLoginForm of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<InheritedLoginForm>()!;
  }

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return false;
  }
}
