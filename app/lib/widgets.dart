import 'package:flutter/material.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          //    DrawerHeader(
          //      child: Text(
          //        'Side menu',
          //        style: TextStyle(color: Colors.white, fontSize: 25),
          //      ),
          //      decoration: BoxDecoration(
          //          color: Colors.green,
          //          image: DecorationImage(
          //              fit: BoxFit.fill,
          //              image: AssetImage('assets/images/cover.jpg'))),
          //    ),
          // ListTile(
          //   leading: const Icon(Icons.nature_people),
          //   title: const Text('About Us'),
          //   onTap: () => {},
          // ),
          // ListTile(
          //   leading: const Icon(Icons.account_box),
          //   title: const Text('Profile'),
          //   onTap: () => {Navigator.of(context).pop()},
          // ),
          ListTile(
            leading: const Icon(Icons.search),
            title: const Text('Search Trees'),
            onTap: () => {Navigator.popAndPushNamed(context, "/search")},
          ),
          ListTile(
            leading: const Icon(Icons.account_circle),
            title: const Text("My trees"),
            onTap: () => {Navigator.popAndPushNamed(context, "/myTrees")},
          ),
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Settings'),
            onTap: () {
              Navigator.popAndPushNamed(context, "/settings");
            },
          ),
          // ListTile(
          //   leading: const Icon(Icons.border_color),
          //   title: const Text('Feedback'),
          //   onTap: () {
          //     Navigator.popAndPushNamed(context, "/searchedTrees");
          //   },
          // ),
          // ListTile(
          //   leading: const Icon(Icons.monetization_on),
          //   title: const Text('Kleine Spende, Bitte'),
          //   onTap: () => {Navigator.of(context).pop()},
          // ),
          ListTile(
            leading: const Icon(Icons.login),
            title: const Text("Login"),
            onTap: () { Navigator.popAndPushNamed(context, "/login"); },
          ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Logout'),
            onTap: () => {Navigator.of(context).pop()},
          ),
        ],
      ),
    );
  }
}
