import 'dart:convert';

import 'package:http/http.dart' as http;

class Globals {
  static String host = 'http://localhost:8080';
}

class AbstractRequest {

  static const Map<String, String> _headers = {
    "Content-Type": "application/json",
    "charset": "utf-8",
    "Access-Control-Allow-Origin" : "*"
  };

  final String serverHost;

  static final Encoding _encoding = Encoding.getByName("utf-8")!;
  
  const AbstractRequest(this.serverHost);
  
  Uri _format(String suffixUrl) {
    return Uri.parse("$serverHost/$suffixUrl");
  }

  Future<http.StreamedResponse> request({
    required String method,
    required String urlSuffix,
    void Function(http.Request)? additionalForRequest,
  }) async {
    http.Request request = http.Request(method, _format(urlSuffix));
    request.encoding = _encoding;
    request.headers.addAll(_headers);

    if (additionalForRequest != null) {
      additionalForRequest(request);
    }

    return request.send();
  }

  Future<http.Response> get({required String urlSuffix}) async {
    return http.get(_format(urlSuffix), headers: _headers);
  }

  Future<http.Response> post({required String urlSuffix, Object? body}) async {
    return http.post(
        _format(urlSuffix),
        headers: _headers,
        encoding: _encoding,
        body: body,
    );
  }

  Future<http.Response> delete({required String urlSuffix, Object? body}) async {
    return http.delete(
      _format(urlSuffix),
      headers: _headers,
      encoding: _encoding,
      body: body
    );
  }
}