import 'dart:convert';

import 'package:tree_share_app/ServerRequests/AbstractRequest.dart';

import 'Tree.dart';

// TODO: use specific method from abstract request
class TreeRequests {
  /// this is the sdk that communicates with our server.
  /// Look at webservice/README to see the documentation
  /// of the REST API of the server.
  late final AbstractRequest _abstractRequest;

  TreeRequests(String serverHost) {
    _abstractRequest = AbstractRequest(serverHost);
  }

  TreeRequests.withGlobalHost() {
    _abstractRequest = AbstractRequest(Globals.host);
  }

  Future<Tree> getById(int id) async {
    String responseBody = await _abstractRequest
        .request(method: "get", urlSuffix: "/trees/$id")
        .then(
      (value) {
        return value.stream.toStringStream().join("");
      },
    );

    return Tree.fromJson(jsonDecode(responseBody));
  }

  Future<Tree> upload(Tree tree) async {
    final responseBody = await _abstractRequest
        .request(
            method: "post",
            urlSuffix: '/trees',
            additionalForRequest: (request) => request.body = jsonEncode(tree))
        .then((value) => value.stream.toStringStream().join(""));

    return Tree.fromJson(jsonDecode(responseBody));
  }

  Future<int> update(Tree tree) async {
    return await _abstractRequest
        .request(
            method: "post",
            urlSuffix: "/trees/${tree.id}",
            additionalForRequest: (request) => request.body = jsonEncode(tree))
        .then((value) => value.statusCode);
  }

  Future<int> deleteById(int id) async {
    return await _abstractRequest
        .request(method: "delete", urlSuffix: "/trees/$id")
        .then((value) => value.statusCode);
  }

  Future<List<Tree>> search(TreeCriteria treeCriteria) async {
    final responseBody = await _abstractRequest
        .request(
            method: 'post',
            urlSuffix: 'trees/search',
            additionalForRequest: (request) =>
                request.body = jsonEncode(treeCriteria))
        .then((value) => value.stream.toStringStream().join(""));

    return (jsonDecode(responseBody) as List<dynamic>)
        .map((e) => Tree.fromJson(e))
        .toList();
  }
}
