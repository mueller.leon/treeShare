enum TreeSpecie {
  APPLE(stringify: "APPLE"),
  PEAR(stringify: "PEAR"),
  OTHER(stringify: "OTHER");

  final String stringify;
  static const Map<String, TreeSpecie> map = {
    "APPLE": TreeSpecie.APPLE,
    "PEAR": TreeSpecie.PEAR,
    "OTHER": TreeSpecie.OTHER
  };

  const TreeSpecie({required this.stringify});

  static TreeSpecie? from(String name) {
    return map[name];
  }

  static String to(TreeSpecie? species) {
    if (species == null) {
      return "";
    } else if (species == TreeSpecie.APPLE ||
        species == TreeSpecie.PEAR ||
        species == TreeSpecie.OTHER) {
      return species.stringify;
    }
    return "";
  }
}

class Tree {
  /// The main DB entity of our server. Also used for backend representation in
  /// this app.
  int? id;
  String? userName;
  String? city;
  TreeSpecie? specie;
  bool? active;
  String? title;
  String? description;
  double? longitude;
  double? latitude;

  // TODO
  // final byte[] image;
  // final DateTime date;

  Tree(
      {this.id,
      this.userName,
      this.city,
      this.specie,
      this.active,
      this.title,
      this.description,
      this.longitude,
      this.latitude});

  factory Tree.fromJson(Map<String, dynamic> json) {
    return Tree(
        id: json['id'],
        userName: json['userName'],
        city: json['city'],
        specie: json['specie'] == null ? null : TreeSpecie.from(json['specie']),
        active: json['active'],
        title: json['title'],
        description: json['description'],
        longitude: json['longitude'],
        latitude: json['latitude']);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'userName': userName,
      'city': city,
      'specie': specie?.stringify,
      'active': active,
      'title': title,
      'description': description,
      'longitude': longitude,
      'latitude': latitude
    };
  }
}

class TreeCriteria {
  /// For searching reasons. Look to the API / TreeRequests
  List<String>? cities;
  List<TreeSpecie>? species;
  List<String>? users;
  bool? active;

  TreeCriteria({this.cities, this.species, this.users, this.active});

  factory TreeCriteria.fromJson(Map<String, dynamic> json) {
    final converted = TreeCriteria();
    converted.active = json['active'];
    converted.cities = json['cities'];
    converted.species = (json['species'] as List<String>?)
        ?.map((e) => TreeSpecie.from(e)!)
        .toList();
    converted.users = json['users'];

    return converted;
  }

  Map<String, dynamic> toJson() {
    return {
      'cities': cities,
      'species': species?.map((e) => e.stringify).toList(),
      'users': users,
      'active': active
    };
  }
}
