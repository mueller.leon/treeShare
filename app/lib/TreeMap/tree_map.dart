import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../ServerRequests/Tree.dart';

class TreeMap extends StatelessWidget {
  /// Shows the positions of the given trees on a map (google maps).
  /// Note: the camera position is automatically set right
  final Completer<GoogleMapController> controller = Completer();
  final List<Tree> trees;

  TreeMap({Key? key, required this.trees})
      : assert(trees.isNotEmpty),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final markerBounds = _markerBounds();

    final center = LatLng(
        (markerBounds.southwest.latitude + markerBounds.northeast.latitude) /
            2,
        (markerBounds.southwest.longitude +
                markerBounds.northeast.longitude) /
            2);

    return GoogleMap(
      mapType: MapType.hybrid,
      initialCameraPosition: CameraPosition(target: center, zoom: 12),
      onMapCreated: _onMapCreated,
      markers: trees
          .map((e) => Marker(
                markerId: MarkerId("${e.id!}: ${e.title!}"),
                position: LatLng(e.latitude!, e.longitude!),
                infoWindow: InfoWindow(title: "${e.id!}: ${e.title!}"),
              ))
          .toSet(),
    );
  }

  void _onMapCreated(GoogleMapController c) async {
    final cameraUpdate = CameraUpdate.newLatLngBounds(_markerBounds(), 50);
    await c.animateCamera(cameraUpdate);

    if (!controller.isCompleted) {
      controller.complete(c);
    }
  }

  /// Calculates the marker bounds of the tree positions.
  /// Needed for right camera position.
  LatLngBounds _markerBounds() {
    final mostSouth = trees.map((e) => e.latitude!).reduce(min);
    final mostEast = trees.map((e) => e.longitude!).reduce(max);
    final mostNorth = trees.map((e) => e.latitude!).reduce(max);
    final mostWest = trees.map((e) => e.longitude!).reduce(min);

    final southWest = LatLng(mostSouth, mostWest);
    final northEast = LatLng(mostNorth, mostEast);

    return LatLngBounds(southwest: southWest, northeast: northEast);
  }
}
