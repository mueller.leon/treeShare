import 'package:flutter/material.dart';
import 'package:tree_share_app/core/global_session_props.dart';

class LoginRequiredButton extends StatelessWidget {
  final Widget child;
  final Function()? onPressed;
  final String? tooltip;

  const LoginRequiredButton(
      {Key? key, required this.child, this.onPressed, this.tooltip})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: sessionUsername == null ? null : onPressed,
      child: child,
      tooltip: sessionUsername == null
          ? "Please login to use this function"
          : tooltip,
    );
  }
}
