import 'dart:js';

import 'package:flutter/material.dart';
import 'package:tree_share_app/Search/search_form_view.dart';
import 'package:tree_share_app/Settings/SettingsView.dart';
import 'package:tree_share_app/TreeMap/tree_map.dart';
import 'package:tree_share_app/login/login_view.dart';
import 'package:tree_share_app/myTrees/my_tree_view.dart';
import 'package:tree_share_app/treesView/searched_trees_view.dart';

import 'Config/tree_config_view.dart';
import 'ServerRequests/Tree.dart';

Map<String, Widget Function(BuildContext)> routes = {
  "/settings": (context) => const SettingsView(),
  "/search": (context) => const SearchFormView(),
  "/login": (context) => const Scaffold(
        body: LoginView(),
      ),
  "/myTrees": (context) => const MyTreesView(),
};
