import 'package:flutter/material.dart';
import 'package:tree_share_app/TreeMap/tree_map.dart';

import '../Config/tree_config_view.dart';
import '../ServerRequests/Tree.dart';

class TreeDetailed extends StatelessWidget {
  final Tree tree;
  const TreeDetailed({Key? key, required this.tree}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        const Card(
          child: ImageIcon(AssetImage(
              "../web/icons/fruitTreeIcon/android-chrome-192x192.png")),
        ),
        Card(
          child: ListTile(
            title: const Text("User"),
            subtitle: Text(tree.userName!),
          ),
        ),
        Card(
          child: ListTile(
            title: const Text("Description"),
            subtitle: Text(tree.description!),
          ),
        ),
        Card(
          child: ListTile(
            title: const Text("City"),
            subtitle: Text(tree.city!),
          ),
        ),
        Card(
          child: ListTile(
            title: const Text("Specie"),
            subtitle: Text(tree.specie!.stringify),
          ),
        ),
        ElevatedButton(
            child: const Icon(Icons.edit),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          TreeConfigView(inputTree: tree, addMode: false)));
            }),
        SizedBox(
          child: TreeMap(
            trees: [tree],
          ),
          height: 300,
        ),
      ],
    );
  }
}
