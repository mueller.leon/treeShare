import 'package:flutter/material.dart';
import 'package:tree_share_app/treeDetailedView/tree_detailed.dart';

import '../ServerRequests/Tree.dart';

class TreeListItem extends StatelessWidget {
  final Tree tree;

  const TreeListItem({Key? key, required this.tree}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return Scaffold(
              appBar: AppBar(
                title: Text(tree.title!),
              ),
              body: TreeDetailed(tree: tree),
            );
          }));
        },
        title: Text(tree.title!),
        leading: const ImageIcon(AssetImage("../web/icons/fruitTreeIcon/android-chrome-192x192.png")),
        subtitle: Row(
          children: [
            Card(child: Text("From: ${tree.userName}"), elevation: 3.5,),
            Card(child: Text("City: ${tree.city}"), elevation: 3.5,),
            Card(child: Text("Specie: ${tree.specie?.stringify}"), elevation: 3.5,)
          ],
        )
      ),
    );
  }
  
}