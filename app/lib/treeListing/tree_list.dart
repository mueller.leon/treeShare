import 'package:flutter/material.dart';
import 'package:tree_share_app/treeListing/tree_list_item.dart';

import '../ServerRequests/Tree.dart';

class TreeList extends StatelessWidget {
  /// Builds a ListView to show all given trees in this list.
  /// Shown information of a tree: title, user, specie, city
  final List<Tree> trees;

  const TreeList({Key? key, required this.trees}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(itemBuilder: (_, index) {
      return TreeListItem(tree: trees[index]);
    },
    itemCount: trees.length,
    );
  }

}