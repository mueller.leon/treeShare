import 'package:flutter/material.dart';
import 'package:tree_share_app/TreeMap/tree_map.dart';
import 'package:tree_share_app/treeListing/tree_list.dart';
import 'package:tree_share_app/widgets.dart';

import '../ServerRequests/Tree.dart';

class _NoTreesFound extends StatelessWidget {
  /// This Widget is shown in the body
  /// of "SearchedTreesView" if no trees were found...
  const _NoTreesFound({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text("Oops, no trees were found..."),
    );
  }
  
}

class SearchedTreesView extends StatefulWidget {
  static final List<Widget Function(List<Tree>)> bodyCandidates = [
    (searchedTrees) => TreeList(trees: searchedTrees),
    (searchedTrees) => TreeMap(trees: searchedTrees)
  ];

  final List<Tree> trees;

  const SearchedTreesView({Key? key, required this.trees}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SearchedTreesViewState();
  }

}

class _SearchedTreesViewState extends State<SearchedTreesView> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Your searched trees"),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(label: "Listing", icon: Icon(Icons.list)),
          BottomNavigationBarItem(label: "Map", icon: Icon(Icons.map))
        ],
        onTap: (newIndex) => setState(() => _currentIndex = newIndex),
        currentIndex: _currentIndex,
      ),
      body: _chooseBody(),
    );
  }
  
  Widget _chooseBody() {
    if (widget.trees.isEmpty) {
      return const _NoTreesFound();
    }
    
    return SearchedTreesView.bodyCandidates[_currentIndex](widget.trees);
  }

}