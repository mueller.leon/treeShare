import 'package:flutter/material.dart';
import 'package:tree_share_app/ServerRequests/AbstractRequest.dart';

class ConnectingToServerWidget extends StatefulWidget {
  const ConnectingToServerWidget({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ConnectingToServerState();
  }
}

class ConnectingToServerSettingItem {
  ConnectingToServerSettingItem(
      {this.isExpanded=false, required this.header, required this.body});

  bool isExpanded;
  final Widget header;
  final Widget body;
}

class ConnectingToServerState extends State<ConnectingToServerWidget> {
  final List<ConnectingToServerSettingItem> _items =
      <ConnectingToServerSettingItem>[
    ConnectingToServerSettingItem(
      header: const Text(
        'Server URI',
        style: TextStyle(fontSize: 20),
      ),
      body: TextFormField(
          onFieldSubmitted: (input) {
            Globals.host = input;
          },
          initialValue: Globals.host,
          decoration: const InputDecoration(
              hintText: 'localhost:8080', border: OutlineInputBorder())),
    )
  ];

  @override
  Widget build(BuildContext context) {
    return ExpansionPanelList(
        expansionCallback: (i, expanded) {
          setState(() {
            _items[i].isExpanded = !_items[i].isExpanded;
          });
        },
        children: _items.map((item) {
          return ExpansionPanel(
              headerBuilder: (context, isOpen) => item.header,
              body: item.body,
              isExpanded: item.isExpanded,
              canTapOnHeader: true);
        }).toList());
  }
}
