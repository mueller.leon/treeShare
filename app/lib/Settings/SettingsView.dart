import 'package:flutter/material.dart';
import 'package:tree_share_app/Settings/ConnectingToServerWidget.dart';

import '../widgets.dart';

class SettingsView extends StatelessWidget {
  const SettingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      appBar: AppBar(title: const Text('Settings')),
      // drawer: Drawer(
      //   child: ListView(
      //     children: [
      //       ConnectingToServerWidget()
      //     ],
      //   ),
      // )
      body: Column(
        children: const [
          ConnectingToServerWidget()
        ],
        // ExpansionPanelList(
        //     children: [
        //       ConnectingToServerWidget().getWidget(),
        //     ],
        //   )
      )
    );
  }
}
