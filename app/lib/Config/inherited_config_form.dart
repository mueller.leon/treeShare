import 'package:flutter/cupertino.dart';
import '../ServerRequests/Tree.dart';
import 'config_form.dart';

class InheritedConfigForm extends InheritedWidget {
  final configFormKey = GlobalKey<FormState>();
  final Tree tree = Tree();
  final Tree? inputTree;
  final bool addMode;

  InheritedConfigForm(
      {Key? key, required this.inputTree, required this.addMode})
      : super(key: key, child: const ConfigForm());

  static InheritedConfigForm of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<InheritedConfigForm>()!;
  }

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return false;
  }
}
