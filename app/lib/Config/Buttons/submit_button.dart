import 'dart:math';

import 'package:flutter/material.dart';
import 'package:tree_share_app/Config/tree_config_view.dart';
import 'package:tree_share_app/ServerRequests/Tree.dart';
import 'package:tree_share_app/ServerRequests/TreeRequests.dart';
import 'package:tree_share_app/core/global_session_props.dart';
import 'package:tree_share_app/myTrees/my_tree_view.dart';

import '../inherited_config_form.dart';

class SubmitButton extends StatelessWidget {
  const SubmitButton({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () async {
        final inheritedParent = InheritedConfigForm.of(context);
        if (inheritedParent.configFormKey.currentState!.validate()) {
          inheritedParent.configFormKey.currentState!.save();

          final addMode = InheritedConfigForm.of(context).addMode;
          final treeConfig = InheritedConfigForm.of(context).tree;

          treeConfig.userName = sessionUsername;
          treeConfig.active = true;

          if (addMode == false) {
            TreeRequests.withGlobalHost().update(treeConfig);
          } else {
            TreeRequests.withGlobalHost().upload(treeConfig);
          }

          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const MyTreesView()));
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Invalid Input')),
          );
        }
      },
      child: const Text('Submit'),
    );
  }
}
