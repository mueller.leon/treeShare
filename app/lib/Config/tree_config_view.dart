import 'package:flutter/material.dart';
import 'package:tree_share_app/ServerRequests/Tree.dart';
import 'package:tree_share_app/widgets.dart';
import 'inherited_config_form.dart';

class TreeConfigView extends StatelessWidget {
  final Tree? inputTree;
  final bool addMode;

  const TreeConfigView({Key? key, this.inputTree, required this.addMode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      appBar: AppBar(
        title: const Text("Configure your Tree"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: InheritedConfigForm(inputTree: inputTree, addMode: addMode),
      ),
    );
  }
}
