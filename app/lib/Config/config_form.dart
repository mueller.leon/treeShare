import 'package:flutter/cupertino.dart';
import 'package:tree_share_app/Config/TextFormFields/description_text_form_field.dart';
import 'package:tree_share_app/Config/TextFormFields/latitude_text_form_field.dart';
import 'package:tree_share_app/Config/TextFormFields/longitude_text_form_field.dart';
import 'package:tree_share_app/Config/TextFormFields/city_text_form_field.dart';
import 'package:tree_share_app/Config/Buttons/submit_button.dart';
import 'package:tree_share_app/ServerRequests/Tree.dart';

import 'TextFormFields/title_text_form_field.dart';
import '../Config/TextFormFields/specie_form_text_field.dart';
import 'inherited_config_form.dart';

class ConfigForm extends StatefulWidget {
  const ConfigForm({Key? key}) : super(key: key);
  @override
  ConfigFormState createState() => ConfigFormState();
}

class ConfigFormState extends State<ConfigForm> {
  Tree tree = Tree();

  //TODO: good validation of input fields + suggestions for cities and species
  @override
  Widget build(BuildContext context) {
    return Form(
      key: InheritedConfigForm.of(context).configFormKey,
      child: Column(
        children: const [
          CityTextFormField(),
          SpecieTextFormField(),
          TitleTextFormField(),
          DescriptionTextFormField(),
          LatitudeTextFormField(),
          LongitudeTextFormField(),
          SubmitButton()
        ],
      ),
    );
  }
}
