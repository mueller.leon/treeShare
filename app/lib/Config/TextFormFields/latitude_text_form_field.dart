import 'package:flutter/material.dart';

import '../../ServerRequests/Tree.dart';
import '../inherited_config_form.dart';

class LatitudeTextFormField extends StatelessWidget {
  const LatitudeTextFormField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Tree? inputTree = InheritedConfigForm.of(context).inputTree;
    return TextFormField(
      initialValue: (inputTree != null && inputTree.latitude != null)
          ? inputTree.latitude.toString()
          : "",
      decoration: const InputDecoration(
        icon: Icon(Icons.location_pin, color: Colors.green),
        hintText: 'Enter latitude',
        labelText: 'Latitude',
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (String? latitude) {
        //TODO: validation
        if (latitude == null) {
          return 'Please enter a latitude value'; // not allowed
        } else if (latitude.isEmpty) {
          return 'Please enter a latitude value'; // not allowed
        } else if (double.tryParse(latitude) == null) {
          return 'Please enter a valid value for latitude like 8.728374'; // has to be parsable to double
        }
        return null; // allowed
      },
      onSaved: (String? latitude) {
        if (latitude == null) {
          return;
        } else if (latitude.isEmpty) {
          return;
        }

        final treeConfig = InheritedConfigForm.of(context).tree;
        treeConfig.latitude ??= double.tryParse(latitude);
      },
    );
  }
}
