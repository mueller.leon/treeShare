import 'package:flutter/material.dart';

import '../../ServerRequests/Tree.dart';
import '../inherited_config_form.dart';

class LongitudeTextFormField extends StatelessWidget {
  const LongitudeTextFormField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Tree? inputTree = InheritedConfigForm.of(context).inputTree;
    return TextFormField(
      initialValue: (inputTree != null && inputTree.longitude != null)
          ? inputTree.longitude.toString()
          : "",
      decoration: const InputDecoration(
        icon: Icon(Icons.location_pin, color: Colors.green),
        hintText: 'Enter longitude',
        labelText: 'Longitude',
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (String? longitude) {
        //TODO: validation
        if (longitude == null) {
          return 'Please enter a longitude value'; // not allowed
        } else if (longitude.isEmpty) {
          return 'Please enter a longitude value'; // not allowed
        } else if (double.tryParse(longitude) == null) {
          return 'Please enter a valid value for longitude like 8.728374'; // has to be parsable to double
        }
        return null; // allowed
      },
      onSaved: (String? longitude) {
        if (longitude == null) {
          return;
        } else if (longitude.isEmpty) {
          return;
        }

        final treeConfig = InheritedConfigForm.of(context).tree;
        treeConfig.longitude ??= double.tryParse(longitude);
      },
    );
  }
}
