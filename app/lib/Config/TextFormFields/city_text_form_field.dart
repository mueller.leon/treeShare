import 'package:flutter/material.dart';

import '../inherited_config_form.dart';

class CityTextFormField extends StatelessWidget {
  const CityTextFormField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: InheritedConfigForm.of(context).inputTree?.city,
      decoration: const InputDecoration(
        icon: Icon(Icons.location_city_outlined, color: Colors.green),
        hintText: 'Enter a city',
        labelText: 'City',
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (String? city) {
        //TODO: validation
        if (city == null) {
          return null; // allowed
        } else if (city.isEmpty) {
          return null; // allowed
        } else if (city.contains(RegExp(r'[0-9]'))) {
          return 'Please enter a valid city name';
        }
        return null; // allowed
      },
      onSaved: (String? city) {
        if (city == null) {
          return;
        } else if (city.isEmpty) {
          return;
        }

        final treeConfig = InheritedConfigForm.of(context).tree;
        treeConfig.city ??= city;
      },
    );
  }
}
