import 'package:flutter/material.dart';

import '../inherited_config_form.dart';

class DescriptionTextFormField extends StatelessWidget {
  const DescriptionTextFormField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: InheritedConfigForm.of(context).inputTree?.description,
      decoration: const InputDecoration(
        icon: Icon(Icons.description, color: Colors.green),
        hintText: 'Enter a description',
        labelText: 'Description',
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (String? description) {
        return null; // description not required
      },
      onSaved: (String? description) {
        if (description == null) {
          return;
        } else if (description.isEmpty) {
          return;
        }

        final treeConfig = InheritedConfigForm.of(context).tree;
        treeConfig.description ??= description;
      },
    );
  }
}
