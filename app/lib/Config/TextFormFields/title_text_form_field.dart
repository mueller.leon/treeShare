import 'package:flutter/material.dart';

import '../inherited_config_form.dart';

class TitleTextFormField extends StatelessWidget {
  const TitleTextFormField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: InheritedConfigForm.of(context).inputTree?.title,
      decoration: const InputDecoration(
        icon: Icon(
            Icons.title,
            color: Colors.green
        ),
        hintText: 'Enter a title',
        labelText: 'Title',
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (String? title) {
        //TODO: validation
        if (title == null) {
          return 'Please enter a title';  // not allowed
        } else if (title.isEmpty) {
          return 'Please enter a title';  // not allowed
        } 
        return null;  // allowed
      },
      onSaved: (String? title) {
        if (title == null) {
          return;
        } else if (title.isEmpty) {
          return;
        }

        final treeConfig = InheritedConfigForm.of(context).tree;
        treeConfig.title ??= title;
      },
    );
  }
}