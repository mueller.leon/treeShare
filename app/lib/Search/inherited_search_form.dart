import 'package:flutter/cupertino.dart';
import 'package:tree_share_app/Search/search_form.dart';
import '../ServerRequests/Tree.dart';

class InheritedSearchForm extends InheritedWidget {
  final searchFormKey = GlobalKey<FormState>();
  final TreeCriteria treeCriteria = TreeCriteria();

  InheritedSearchForm({Key? key})
      : super(key: key, child: const SearchForm());

  static InheritedSearchForm of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<InheritedSearchForm>()!;
  }

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return false;
  }
}
