import 'package:flutter/material.dart';
import 'package:tree_share_app/Search/search_form.dart';
import 'package:tree_share_app/Search/search_form_view.dart';
import 'package:tree_share_app/treesView/searched_trees_view.dart';
import 'package:tree_share_app/widgets.dart';
import '../ServerRequests/Tree.dart';

class Search extends StatelessWidget {
  final List<Tree> trees;

  const Search({Key? key, required this.trees}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      appBar: AppBar(
        title: const Text('Search Trees'),
      ),
      body: Column(
        children: const <Widget>[
          SearchForm(),
          //SearchedTreesView(trees: trees),
        ],
      ),
    );
  }
}
