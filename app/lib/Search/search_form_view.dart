import 'package:flutter/material.dart';
import 'package:tree_share_app/widgets.dart';
import 'inherited_search_form.dart';

class SearchFormView extends StatelessWidget {
  const SearchFormView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      appBar: AppBar(
        title: const Text("Search Trees"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: InheritedSearchForm(),
      ),
    );
  }
}
