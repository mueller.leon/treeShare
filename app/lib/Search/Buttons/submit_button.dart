import 'package:flutter/material.dart';
import 'package:tree_share_app/Search/inherited_search_form.dart';
import 'package:tree_share_app/ServerRequests/Tree.dart';
import 'package:tree_share_app/ServerRequests/TreeRequests.dart';
import 'package:tree_share_app/treesView/searched_trees_view.dart';

class SubmitButton extends StatelessWidget {
  const SubmitButton({Key? key}) : super(key: key);@override

  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () async {
        final inheritedParent = InheritedSearchForm.of(context);
        if (inheritedParent.searchFormKey.currentState!.validate()) {
          inheritedParent.treeCriteria.cities = null;
          inheritedParent.treeCriteria.species = null;
          inheritedParent.treeCriteria.users = null;
          inheritedParent.treeCriteria.active = true;

          inheritedParent.searchFormKey.currentState!.save();
          final treeCriteria = InheritedSearchForm.of(context).treeCriteria;
          
          final treesInfo = await TreeRequests.withGlobalHost().search(treeCriteria);
          final trees = <Tree> [];
          for (final treeInfo in treesInfo) {
            trees.add(
                await TreeRequests.withGlobalHost().getById(treeInfo.id!)
            );
          }
          Navigator.push(context, MaterialPageRoute(builder: (context) => SearchedTreesView(trees: trees)));
        }
        else {
          ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text(
                'You didn\' t enter any valid search terms....')),
          );
        }
      },
      child: const Text('Submit'),
    );
  }

}