import 'package:flutter/material.dart';
import 'package:tree_share_app/ServerRequests/Tree.dart';
import '../../Presentation/app_icons.dart';
import '../inherited_search_form.dart';

class SpecieTextFormField extends StatelessWidget {
  const SpecieTextFormField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: const InputDecoration(
          icon: Icon(
              AppIcons.fruit_svgrepo_com,
              color: Colors.green
          ),
          hintText: 'Enter your desired species of tree',
          labelText: 'Species of tree',
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (String? specie) {
        //TODO: validation
        if (specie == null) {
          return null;
        } else if (specie.isEmpty) {
          return null;
        } else if (TreeSpecie.from(specie) != null) {
          return null;
        }

        return 'Specie not available. Possible values: ' +
            TreeSpecie.values.map((e) => e.stringify).join(", ");
      },
      onSaved: (String? specie) {
        if (specie == null) {
          return;
        } else if (specie.isEmpty) {
          return;
        }

        TreeCriteria criteria = InheritedSearchForm.of(context).treeCriteria;
        criteria.species ??= <TreeSpecie> [];
        criteria.species!.add(TreeSpecie.from(specie)!);
      },
    );
  }
}