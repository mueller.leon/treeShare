import 'package:flutter/material.dart';
import '../inherited_search_form.dart';

class UserTextFormField extends StatelessWidget {
  const UserTextFormField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: const InputDecoration(
          icon: Icon(
              Icons.person_outline,
              color: Colors.green
          ),
          hintText: 'Enter a the username of a tree owner or creator',
          labelText: 'Owner / Creator'
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: (userName) {
        //TODO: validation
        return null;
      },
      onSaved: (String? userName) {
        if (userName == null) {
          return;
        } else if (userName.isEmpty) {
          return;
        }

        final criteria = InheritedSearchForm.of(context).treeCriteria;
        criteria.users ??= <String> [];
        criteria.users!.add(userName);
      },
    );
  }
}