import 'package:flutter/material.dart';
import 'package:tree_share_app/Search/TextFormFields/city_text_form_field.dart';
import 'package:tree_share_app/Search/TextFormFields/specie_form_text_field.dart';
import 'package:tree_share_app/Search/TextFormFields/user_text_form_field.dart';
import 'package:tree_share_app/Search/Buttons/submit_button.dart';
import '../ServerRequests/Tree.dart';
import 'inherited_search_form.dart';

class SearchForm extends StatefulWidget {
  const SearchForm({Key? key}) : super(key: key);
  @override
  SearchFormState createState() => SearchFormState();
}

class SearchFormState extends State<SearchForm> {
  TreeCriteria treeCriteria = TreeCriteria();

  //TODO: validation of input fields + suggestions for cities and species
  @override
  Widget build(BuildContext context) {
    return Form(
      key: InheritedSearchForm.of(context).searchFormKey,
      child: Column(children:  const [
            CityTextFormField(),
            SpecieTextFormField(),
            UserTextFormField(),
            SubmitButton()
        ],
      ),
    );
  }
}

//   /// add / remove button for multi search form fields
//   Widget _addRemoveButton(List<dynamic> searchFieldList, bool add, int index) {
//     return InkWell(
//       onTap: () {
//         if (add) {
//           // add new text-fields at the top of all multi textfields
//           searchFieldList.insert(0, null);
//         } else {
//           searchFieldList.removeAt(index);
//         }
//         setState(() {});
//       },
//       child: Container(
//         width: 30,
//         height: 30,
//         decoration: BoxDecoration(
//           color: (add) ? Colors.green : Colors.red,
//           borderRadius: BorderRadius.circular(20),
//         ),
//         child: Icon(
//           (add) ? Icons.add : Icons.remove,
//           color: Colors.white,
//         ),
//       ),
//     );
//   }
// }