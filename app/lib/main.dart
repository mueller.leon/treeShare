import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tree_share_app/Search/search_form_view.dart';
import 'package:tree_share_app/routes.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

void main() {
  if (defaultTargetPlatform == TargetPlatform.android) {
    AndroidGoogleMapsFlutter.useAndroidViewSurface = true;
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TreeShare',
      theme: ThemeData(


        primarySwatch: Colors.green,
      ),
      home: const SearchFormView(),
      routes: routes,
    );
  }
}