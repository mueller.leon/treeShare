# Requirements
- Maven Plugin
- Java JDK 16 or later

# Build the server application
To Build the server application you have to call
```
./mvnw clean package spring-boot:repackage
```
in this dir.
This will build a JAR that you can run with java -jar <YOUR JAR FILE>.
The Jar is in the build dir.

# Run the server

To run Application enter in your (Git Bash) Terminal the following command: \
`./mvnw spring-boot:run`\
If you see something like this the server started correctly:

        [...]
        .   ____          _            __ _ _
        /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
        ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
        \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
        '  |____| .__|_| |_|_| |_\__, | / / / /
        =========|_|==============|___/=/_/_/_/
        :: Spring Boot ::                (v2.6.1)
        
        2021-12-02 17:14:29.017  INFO 14144 --- [           main] t.webservice.WebserviceApplication       : Starting WebserviceApplication using Java 17.0.1 on XXXX with PID 14144 (C:\...\treeShare\webservice\target\classes started by XXX in C:\...\treeShare\webservice)
        2021-12-02 17:14:29.021  INFO 14144 --- [           main] t.webservice.WebserviceApplication       : No active profile set, falling back to default profiles: default
        2021-12-02 17:14:30.775  INFO 14144 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
        2021-12-02 17:14:30.797  INFO 14144 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
        2021-12-02 17:14:30.797  INFO 14144 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.55]
        2021-12-02 17:14:30.965  INFO 14144 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
        2021-12-02 17:14:30.965  INFO 14144 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1852 ms
        2021-12-02 17:14:31.529  INFO 14144 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
        2021-12-02 17:14:31.541  INFO 14144 --- [           main] t.webservice.WebserviceApplication       : Started WebserviceApplication in 3.15 seconds (JVM running for 3.735)

Now you can direct to http://localhost:8080/test/hello in your browser to view the test page.
After this setup up you can run the start the server normally over "Run".

Possible Errors:

If you get the "ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.", you need to set the environment variable to your Java jre (https://fluttercorner.com/solved-error-java_home-is-not-set-and-no-java-command-could-be-found-in-your-flutter-path-in-flutter/).

# API Documentation and manual integration tests with Swagger
If you run this server locally you can type `http://localhost:8080/swagger-ui.html` into your browser.
Now you should see the Swagger-UI with all available API endpoints: \
![img.png](../docs/imgs/swagger.png)

# Testing
Call in this directory
```
./mvnw test
```
to start the unit tests. There should be no Failures/Errors.

