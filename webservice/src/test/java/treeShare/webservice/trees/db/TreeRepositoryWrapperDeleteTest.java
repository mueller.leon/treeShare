package treeShare.webservice.trees.db;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import treeShare.webservice.trees.db.model.Tree;
import treeShare.webservice.trees.db.model.TreeRepository;
import treeShare.webservice.trees.db.model.TreeSpecie;
import treeShare.webservice.trees.db.model.search.TreeCriteria;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TreeRepositoryWrapperDeleteTest {
    @Autowired
    TreeRepositoryWrapper repo;
    @Autowired
    TreeRepository coreRepo;  // to delete the fake trees
    Tree savedTree;

    @BeforeAll
    void beforeAll() {
        coreRepo.deleteAll();
    }
    
    @BeforeEach
    void beforeEach() {
        savedTree = repo.saveNew(
                Tree.builder()
                        .city("Test")
                        .userName("Test")
                        .active(false)
                        .specie(TreeSpecie.APPLE)
                        .longitude(0.0)
                        .latitude(0.0)
                        .description("Test")
                        .title("Test")
                        .build()
        );
    }

    @Test
    void delete_should_DeleteSuccessfully() {
        var successful = repo.deleteById(savedTree.getId());
        assertTrue(successful);
        assertTrue(repo.findById(savedTree.getId()).isEmpty());
    }

    @Test
    void delete_should_DeleteNotSuccessful(){
        repo.deleteById(savedTree.getId());
        var unsuccessful = repo.deleteById(savedTree.getId());
        assertFalse(unsuccessful);
    }
}