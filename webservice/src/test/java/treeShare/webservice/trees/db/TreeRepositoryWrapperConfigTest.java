package treeShare.webservice.trees.db;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import treeShare.webservice.trees.db.model.Tree;
import treeShare.webservice.trees.db.model.TreeRepository;
import treeShare.webservice.trees.db.model.TreeSpecie;
import treeShare.webservice.trees.db.model.search.TreeCriteria;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TreeRepositoryWrapperConfigTest {
    @Autowired
    TreeRepositoryWrapper repo;
    @Autowired
    TreeRepository coreRepo;  // to delete the fake trees
    Tree savedTree;
    TreeCriteria criteria;

    @BeforeAll
    void beforeAll() {
        coreRepo.deleteAll();
        savedTree = repo.saveNew(
            Tree.builder()
                    .city("Test")
                    .userName("Test")
                    .active(false)
                    .specie(TreeSpecie.APPLE)
                    .longitude(0.0)
                    .latitude(0.0)
                    .description("Test")
                    .title("Test")
                    .build()
        );
    }

    @Test
    void saveNew_city_should_be_equal() {
        assertThat(savedTree.getCity(), equalTo("Test"));
    }

    @Test
    void saveNew_userName_should_be_equal() {
        assertThat(savedTree.getUserName(), equalTo("Test"));
    }

    @Test
    void saveNew_active_should_be_equal() {
        assertThat(savedTree.getActive(), equalTo(false));
    }

    @Test
    void saveNew_specie_should_be_equal() {
        assertThat(savedTree.getSpecie(), equalTo(TreeSpecie.APPLE));
    }

    @Test
    void saveNew_longitude_should_be_equal() {
        assertThat(savedTree.getLongitude(), equalTo(0.0));
    }

    @Test
    void saveNew_latitude_should_be_equal() {
        assertThat(savedTree.getLatitude(), equalTo(0.0));
    } 

    @Test
    void saveNew_description_should_be_equal() {
        assertThat(savedTree.getDescription(), equalTo("Test"));
    }

    @Test
    void saveNew_title_should_be_equal() {
        assertThat(savedTree.getTitle(), equalTo("Test"));
    }
}