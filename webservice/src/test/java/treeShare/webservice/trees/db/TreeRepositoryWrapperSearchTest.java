package treeShare.webservice.trees.db;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import treeShare.webservice.trees.db.model.Tree;
import treeShare.webservice.trees.db.model.TreeRepository;
import treeShare.webservice.trees.db.model.TreeSpecie;
import treeShare.webservice.trees.db.model.search.TreeCriteria;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TreeRepositoryWrapperSearchTest {
    @Autowired
    TreeRepositoryWrapper repo;
    @Autowired
    TreeRepository coreRepo;  // to delete the fake trees
    Tree savedTree;
    TreeCriteria criteria;

    @BeforeAll
    void beforeAll() {
        coreRepo.deleteAll();
        savedTree = repo.saveNew(
                Tree.builder()
                        .city("Test")
                        .userName("Test")
                        .active(false)
                        .specie(TreeSpecie.APPLE)
                        .longitude(0.0)
                        .latitude(0.0)
                        .description("Test")
                        .title("Test")
                        .build()
        );
    }

    @BeforeEach
    void beforeEach() {
        criteria = new TreeCriteria();
    }

    @Test
    void noCriteria_should_returnAllTrees() {
        var searchResult = repo.search(criteria);
        assertThat(searchResult.size(), equalTo(1));
        assertThat(searchResult.get(0), equalTo(savedTree));
    }

    @Test
    void wrongCityInCriteria_should_returnEmptyResult() {
        this.criteria.setCities(List.of("WrongCity"));

        assertTrue(repo.search(criteria).isEmpty());
    }

    @Test
    void rightCityInCriteria_should_returnTreeSample() {
        this.criteria.setCities(List.of("Test"));

        var searchResult = repo.search(criteria);
        assertThat(searchResult.size(), equalTo(1));
        assertThat(searchResult.get(0), equalTo(this.savedTree));
    }

    @Test
    void nonActiveTreeCriteria_should_returnTreeSample() {
        this.criteria.setActive(false);

        var searchResult = repo.search(criteria);
        assertThat(searchResult.size(), equalTo(1));
        assertThat(searchResult.get(0), equalTo(savedTree));
    }

    @Test
    void activeTreeCriteria_should_returnEmptyResult() {
        criteria.setActive(true);

        assertTrue(repo.search(criteria).isEmpty());
    }
}
