package treeShare.webservice.trees.db.model.search;

import org.springframework.data.jpa.domain.Specification;
import treeShare.webservice.trees.db.model.Tree;
import treeShare.webservice.trees.db.model.TreeSpecie;

import javax.persistence.criteria.Predicate;
import java.sql.Date;
import java.util.List;

public final class TreeSpecification {

    public static Specification<Tree> withSpecies(List<TreeSpecie> species) {
        if (species == null)
            return emptySpecification();

        return (root, query, builder) -> builder.or(
                species.stream()
                        .map(specie -> builder.equal(root.get("specie"), specie))
                        .toArray(Predicate[]::new)
        );
    }

    public static Specification<Tree> withCities(List<String> cities) {
        if (cities == null)
            return emptySpecification();

        return (root, query, builder) -> builder.or(
                cities.stream()
                        .map(city -> builder.equal(root.get("city"), city))
                        .toArray(Predicate[]::new)
        );
    }

    public static Specification<Tree> withUsers(List<String> userNames) {
        if (userNames == null)
            return emptySpecification();

        return (root, query, builder) -> builder.or(
                userNames.stream()
                        .map(userName -> builder.equal(root.get("userName"), userName))
                        .toArray(Predicate[]::new)
        );
    }

    public static Specification<Tree> fromAvailabilityDate(Date date) {
        if (date == null)
            return emptySpecification();

        return (root, query, builder) -> builder.greaterThanOrEqualTo(
          root.get("specie"),
          date
        );
    }

    public static Specification<Tree> isActive(Boolean active) {
        if (active == null)
            return emptySpecification();

        return (root, query, builder) -> builder.equal(root.get("active"), active);
    }

    private static Specification<Tree> emptySpecification() {
        return Specification.where(null);
    }
}
