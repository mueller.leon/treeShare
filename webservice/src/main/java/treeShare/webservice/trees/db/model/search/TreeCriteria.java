package treeShare.webservice.trees.db.model.search;

import lombok.Getter;
import lombok.Setter;
import treeShare.webservice.trees.db.model.TreeSpecie;

import java.sql.Date;
import java.util.List;

@Getter
@Setter
public class TreeCriteria {
    private List<String> cities;
    private List<TreeSpecie> species;
    private List<String> users;
    private Date availableFrom;
    private Boolean active;
}
