package treeShare.webservice.trees.db.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.sql.Date;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
/**
 * This is the entity of our DB. TreeRepository(Wrapper) manages the rest with the DB for us :)
 */
public class Tree {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String userName;

    @NotNull
    private String city;

    @NotNull
    private TreeSpecie specie;

    @NotNull
    private Boolean active;

    private String title;
    private String description;
    private Double longitude;
    private Double latitude;
    private Date availableFrom;
    private byte[] image;
}
