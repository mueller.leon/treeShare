package treeShare.webservice.trees.db;

import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import treeShare.webservice.trees.db.model.search.TreeCriteria;
import treeShare.webservice.trees.db.model.search.TreeSpecification;
import treeShare.webservice.trees.db.model.Tree;
import treeShare.webservice.trees.db.model.TreeRepository;

import java.util.List;
import java.util.Optional;

@Component
/**
 * A Wrapper that extends the core TreeRepository with our required functionalities.
 * You should use this repo instead of the core TreeRepository.
 */
public class TreeRepositoryWrapper {
    private TreeRepository repo;

    @Autowired
    public TreeRepositoryWrapper(
            TreeRepository repo
    ) {
        this.repo = repo;
    }

    public boolean deleteById(@NonNull Long id) {
        if (!this.repo.existsById(id))
            return false;

        this.repo.deleteById(id);
        return true;
    }

    public Tree saveNew(@NonNull Tree tree) {
        tree.setId(null);
        return this.repo.save(tree);
    }

    public boolean update(@NonNull Tree tree) {
        if (!this.repo.existsById(tree.getId()))
            return false;

        this.repo.save(tree);
        return true;
    }

    public Optional<Tree> findById(@NonNull Long id) {
        return this.repo.findById(id);
    }

    public List<Tree> search(TreeCriteria treeCriteria) {
        return this.repo.findAll(
                TreeSpecification.fromAvailabilityDate(treeCriteria.getAvailableFrom())
                        .and(TreeSpecification.isActive(treeCriteria.getActive()))
                        .and(TreeSpecification.withSpecies(treeCriteria.getSpecies()))
                        .and(TreeSpecification.withCities(treeCriteria.getCities()))
                        .and(TreeSpecification.withUsers(treeCriteria.getUsers()))
        );
    }

    public Page<Tree> search(TreeCriteria treeCriteria, Pageable pageable) {
        return this.repo.findAll(
                TreeSpecification.fromAvailabilityDate(treeCriteria.getAvailableFrom())
                        .and(TreeSpecification.isActive(treeCriteria.getActive()))
                        .and(TreeSpecification.withSpecies(treeCriteria.getSpecies()))
                        .and(TreeSpecification.withCities(treeCriteria.getCities()))
                        .and(TreeSpecification.withUsers(treeCriteria.getUsers()))
                ,
                pageable
        );
    }
}
