package treeShare.webservice.trees.db.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import treeShare.webservice.trees.db.TreeRepositoryWrapper;

import java.sql.Date;
import java.time.LocalDate;

@Configuration
/**
 * In order to save fake trees in DB at start of the server (In-Memory DB => no persistence!).
 */
public class FakeTrees {
    private TreeRepositoryWrapper repo;

    @Autowired
    public FakeTrees(TreeRepositoryWrapper repo) {
        this.repo = repo;

        this.feedDBWithFakeTrees();
    }

    private void feedDBWithFakeTrees() {
        this.repo.saveNew(
                Tree.builder()
                        .active(true)
                        .city("Freiburg")
                        .userName("Leon")
                        .title("Das ist Leons Baum")
                        .latitude(48.00601299791434)
                        .longitude(7.795139587403903)
                        .description("Kein Bier vor vier")
                        .specie(TreeSpecie.APPLE)
                        .availableFrom(Date.valueOf(LocalDate.of(2021, 8, 31)))
                        .build()
        );

        this.repo.saveNew(
                Tree.builder()
                        .active(true)
                        .specie(TreeSpecie.PEAR)
                        .userName("Alice")
                        .city("Kirchzarten")
                        .latitude(47.96602954019403)
                        .longitude(7.948948181153903)
                        .title("Das ist Alices Baum")
                        .description("Meine Brinen sind toll")
                        .availableFrom(Date.valueOf(LocalDate.of(2022, 1, 13)))
                        .build()
        );

        this.repo.saveNew(
                Tree.builder()
                        .userName("Marvin")
                        .city("Ettenheim")
                        .title("Das ist Marvins Baum")
                        .active(true)
                        .latitude(48.25906156044188)
                        .longitude(7.789362762573249)
                        .description("Bärte sind toll")
                        .specie(TreeSpecie.OTHER)
                        .availableFrom(Date.valueOf(LocalDate.of(2020, 5, 1)))
                        .build()
        );

        this.repo.saveNew(
                Tree.builder()
                        .availableFrom(Date.valueOf(LocalDate.of(2022, 5, 1)))
                        .specie(TreeSpecie.APPLE)
                        .userName("Öffentlich")
                        .city("Karlsruhe")
                        .latitude(49.012293718737155)
                        .longitude(8.428629730346685)
                        .active(true)
                        .title("Öffentlicher Baum")
                        .description("Test123")
                        .build()
        );

        this.repo.saveNew(
                Tree.builder()
                        .availableFrom(Date.valueOf(LocalDate.of(2022, 5, 1)))
                        .specie(TreeSpecie.PEAR)
                        .userName("Öffentlich")
                        .city("Karlsruhe")
                        .latitude(48.999231310809186)
                        .longitude(8.401850555541998)
                        .active(false)
                        .title("Öffentlicher Baum")
                        .description("Test123")
                        .build()
        );

        this.repo.saveNew(
                Tree.builder()
                        .specie(TreeSpecie.OTHER)
                        .latitude(48.9991)
                        .longitude(8.401)
                        .active(true)
                        .userName("Abe")
                        .city("Karlsruhe")
                        .title("Das ist Abe's Baum")
                        .description("Der coolste Baum aller Zeiten")
                        .build()
        );
    }
}
