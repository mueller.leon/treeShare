# Documentation of the tree REST API
The following chapters describe all available Endpoints of the TreeController
Alternatively you can use the swagger-ui for documentation (see [here](../../../../../../README.md#API%20Documentation%20with%20Swagger)).

## `POST /trees`
Upload a new tree. The properties of the tree are given in Request Body in JSON-format.
Please see trees/db/model/Tree.java for all possible properties
of a tree. Example of the request body :
```
{
    "userName" : "Test",
    "city" : "Karlsruhe",
    "active" : true,
    "specie" : "APPLE"
}
```

## `POST /trees/{id}`
Update a tree with the given id. The new tree properties are
located in the request body. Properties with value=null are ignored
(the old value of a property stays).\
Example:
```
{
    "active" : false,
    "specie" : "PEAR"
}
```

## `GET /trees/{id}`
Get a tree by id. The response contains all stored information
of the tree.

## `DELETE /trees/{id}`
Delete the tree which has the given id.

## `POST /trees/search`
Search trees which fulfill the given TreeCriteria.
The tree criteria are given in the request body.
If a criteria has the value=null, it is ignored for the search.
The Response is a list with all trees.
Please visit trees/db/model/search/TreeCriteria.java
to see all possible search criteria properties. \
Example of a request body:
```
{
    "species" : ["APPLE", "PEAR"],
    "active" : true,
    "cities" : ["Karlsruhe", "Freiburg"]
    "users" : "test"
}
```

## `POST /trees/search/paged`
The same as above, but we are using pages (for efficiency).
The request body is a SearchRequest. Please visit trees/controller/model/search/SearchRequest.java
for all possible properties. The Response is a Page/Slice of tree ids which fulfill the criteria. \
Example of a request body:
```
{
    "treeCriteria" : {
        "cities" : "Karlsruhe"
    },
    "paging" : {
        "pageNumber" : 10,
        "pageSize" : 100,
        "sortBy" : ["id", "userName"]
    }
}
```