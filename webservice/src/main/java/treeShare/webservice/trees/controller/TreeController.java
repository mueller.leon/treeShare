package treeShare.webservice.trees.controller;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import treeShare.webservice.trees.db.TreeRepositoryWrapper;
import treeShare.webservice.trees.controller.model.search.SearchRequest;
import treeShare.webservice.trees.controller.model.search.TreeInfo;
import treeShare.webservice.trees.db.model.Tree;
import treeShare.webservice.trees.db.model.search.TreeCriteria;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/trees", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class TreeController {
    private TreeRepositoryWrapper repo;

    @Autowired
    public TreeController(TreeRepositoryWrapper repo) {
        this.repo = repo;
    }

    @PostMapping
    public Tree upload(@RequestBody Tree tree) {
        // TODO: check if valid properties of tree!
        return this.repo.saveNew(tree);
    }

    @PostMapping("/{id}")
    public void update(@PathVariable Long id, @RequestBody Tree tree) {
        tree.setId(id);
        if (!this.repo.update(tree))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        if (!this.repo.deleteById(id))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{id}")
    public Tree getById(@PathVariable Long id) {
        return this.repo.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND)
        );
    }

    @ApiOperation(
            value = "Search for trees in the DB with the given criteria of tree.",
            notes = "If a member of criteria is null this means, that \"you don't search for that special criteria\":\n" +
            "E.g. if cities == null: don't filter trees by cities.\n" +
            "The lists in criteria allows to search for multiple values.\n" +
            "E.g. cities = [\"Freiburg\", \"Karlsruhe\"] => all trees with city == \"Freiburg\" OR city == \"Karlsruhe\"."
    )
    @PostMapping("/search")
    public List<TreeInfo> search(@RequestBody TreeCriteria treeCriteria) {
        return this.repo.search(treeCriteria)
                .stream()
                .map(tree -> new TreeInfo(tree.getId()))
                .toList();
    }

    @PostMapping("/search/paged")
    public Slice<TreeInfo> search(@RequestBody SearchRequest searchRequest) {
        return this.repo.search(
                searchRequest.getTreeCriteria(),
                PageRequest.of(
                        searchRequest.getPaging().getPageNumber(),
                        searchRequest.getPaging().getPageSize(),
                        Sort.by(searchRequest.getPaging().getSortBy().toArray(String[]::new))
                )
        ).map(tree -> new TreeInfo(tree.getId()));
    }

}
