package treeShare.webservice.trees.controller.model.search;

import lombok.*;
import treeShare.webservice.trees.db.model.search.TreeCriteria;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SearchRequest {

    @Builder.Default
    private TreeCriteria treeCriteria = new TreeCriteria();

    @Builder.Default
    private Paging paging = new Paging();
}
