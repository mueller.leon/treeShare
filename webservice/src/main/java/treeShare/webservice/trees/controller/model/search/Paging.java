package treeShare.webservice.trees.controller.model.search;

import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class Paging {
    private List<String> sortBy = Arrays.asList("id");
    private int pageNumber = 0;
    private int pageSize = 10;
}
