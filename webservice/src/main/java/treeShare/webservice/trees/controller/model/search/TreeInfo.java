package treeShare.webservice.trees.controller.model.search;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class TreeInfo {
    @NonNull
    private Long id;
}
