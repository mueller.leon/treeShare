# treeShare

## Getting started
Open your bash terminal and navigate to webservice directory. Run
```
./mvnw spring-boot:run
```
to start the server. The app will communicate with the server.


Open another bash terminal and navigate to app directory. Run
```
flutter run -d chrome
```
to start the app. This will open the app in the Google Chrome Browser.

## Leitfragen Verlinkungen

### 1 Anforderungsanalyse
siehe Verzeichnis docs/Anforderungsanalyse
1. Anforderungserfassung in natürlicher Sprache => z.B. Satzschablonen oder User Stories
	| siehe Docs/Anforderungsanalyse in Repo
2. Ziele und Zielgrupen festhalten => z.B. Produkt Vision Board / Personas formulieren
	| siehe Docs/Anforderungsanalyse in Repo
3. Anforderungen gliedern => Epics / User Story Map
	| siehe Docs/Anforderungsanalyse in Repo
4. Pitch / Kickoff Präsentation/Disskussion
	| Präsentation in Vorlesung
5. grobes zeitliches Schätzen der Anforderungen => in Personentagen oder in UserStoryPoints / FunctionPoints
	| nun in Repo hochgeladen unter Docs/Anforderungsanalyse/AufwandZeit	

#### **Bonus:**

1. Eventstorming Board Phase 1 und 2 (Ermitteln der Domänenevents)
	Phase 1 s. Docs/Anforderungsanalyse in Repo
2. Glossar für Fachbegriffe
	Repo Docs/Anforderungsanalyse

### 2 Systementwurf
=> Docs/Systementwurf

1. Festhalten der Technologieentscheidungen
    enthaten
2. Use-Case Diagramme für grobes Verhalten des Systems
    enthalten
3. Aktivitätsdiagramm pro Teamperson eins oder ein ausführliches Zustandsdiagramm, falls besser für Projekt geeignet
    Für die Aktivitäten: 
        **Search** - unter allen Bäumen filtern und anzeigen
        **MyTrees** - Baum anlegen
                    - Baum bearbeiten
4. Klassendiagramm: 
	nur Entity Klassen
5. Schnittstellen Web/Persistenz/Ui visualisieren => Komponentendiagramm	
	enthalten
6. Sequenzdiagram
	enthalten

#### **Bonus:**

1. Mockups/Wireframes der UI
	=> Docs/Anforderungsanalyse/Wireframe.pdf

### 3 Implementierung

1. Code-Review => Naming Variablen / Funktionen / Klassen
    - Namenskonventionen: 
		- Files in snake_case
		- Classes UpperCamelCase
		- Variablen, Konstanten, Params in lowerCamelCase
	- Typisierung von Variablen und Klassenattributen
	- "new" Keyword in Widgets vermieden
	- "const" Keyword für Widgets verwenden, die sich nicht ändern 
2. Code-Review => SOLID Prinzipien
	- Single Responsibility Priciple => Verschiedene Tree Klassen für Speichern (Model), Suche und Config
	- Open/Closed Principle => siehe z.B. TreeSpecification.java in webservice/src/<...>/trees/db/model/search: Die Klasse ist offen gegenüber Erweiterungen (durch hinzufügen einer neuer Funktion), aber geschlossen gegenüber Veränderungen (die einzelnen Specifications müssen nicht angepasst werden, wenn eine Erweiterung stattfindet). 
	- Liskov Substitution Principle => Keine Anwendung, da keine Vererbung
	- Interface Segregation Principle => Keine Interfaces
	- Dependency Inversion Principle => Nicht angewendet, weder Interfaces noch Vererbung
3. Code-Review => DRY
	- Baumkonfigurator gleich für add und update, nur dass Daten geladen werden bei Edit 
			=> nicht zwei ähnliche Konfiguratoren
	- etc.
4. Code-Review => Error-Handling
	- Parsen absichern
	- Nullwerte immer abfangen
	- Aussagekräftige Fehlermeldungen werfen
	- Eingabevalidierung

Bei jeder Aufgabe wurde ein Mergerequest (in den master branch) gestellt. Dort sind wir zusammen mündlich den Code durchgegangen (Review) und haben ihn nach den erlernten Prinzipien verbessert. Danach wurde dieser in den master Branch gemerget. => Siehe Merge Requests und die Commits bzw. den Code.

### 4 Test

1. Manuelles Testkonzept => kurze Schrittanleitung für manuelle Tests
	Siehe Anleitung in webservice/README.md (=> Unittests lokal ausführen). Zuzüglich manuell getestet über Postman/Swagger-UI mit manuellen Eingaben. => Hat für unser Projekt gereicht.
2. Unit Tests => Pro Person mindestens eine Klasse / Funktionalität testen
	Siehe Unittests in webservice/src/test.
3. Integrationstest oder Ende-zu-Ende Test gegen eine Persistenz oder eine API
	Integrationtests wurden nicht explizit geschrieben. Wie gesagt wurden hierfür hauptsächlich manuelle Tests durchgeführt (Postman / Swagger-UI).

### 5 Build / Deploy / Maintanance

1. Buildkonzept: 
    - Wie wird der Build getriggert?
		Manuell und lokal, siehe README.md in app/ und webservice/\
	- Was wird gebaut?\
		siehe README.md in app/ und webservice/\
	- Wo wird gebaut?
    	lokal auf dem PC (ausreichend für uns)
	- Wann/Wie oft werden welche Tests ausgeführt?\
		Bei Änderungen am Webservice: Immer lokale Ausführung der Unittests. Sonst manuelle / lokale Tests der veränderten Fuktionen.\
	- Wer wird wann informiert Entwickler/Kunde/DevOps\
		Entwickler: Schreiben in den Chat über Änderungen / beim Treffen über Änderungen informiert.\
		Kunde: Sobald es eine neue App-Version gibt und sie sich diese herunterladen sollten.
    
2. Script für Semantic Versioning erstellen
Nicht umgesetzt aber angedacht:
Version x.y.z => x = major version für keine Rückwärtskompatibilität; y = minor version (Rückwärtskompatibel, Erweiterung der Funktionalitäten); z = patch version (Rückwärtskompatibel, Keine Erweiterungen, Bugfix).
Version releases durch git tags nach jedem Merge Request in den master.

3. Testautomatisierung in der Builpipeline
Nicht möglich, da Codeberg keine CI/CD ermöglicht... Umstellung auf etwas anderes wäre zu aufwendig, da in Codeberg schon viel vorhanden war.
Mit einem Befehl können dennoch alle Tests auf einmal gestartet werden.

### 6 Querschnittsthemen

1. Projektmanagement
- Teamaufgaben\
Leon hat sich um das Backend gekümmert (server, DB und Anbindung an die App). Am Ende allerdings auch in der App unterstützt, da die Zeit knapp wurde.\
Alice hat sich um die App/UI gekümmert.\
Marvin möchte die Dokumentation unseres Projekts übernehmen.\
- Vorgehen\
Agiles Vorgehen nach Scrum. Sprintzyklus ging ungefähr immer 2 Wochen. Tasks und Sprints sind in Codeberg eingetragen.\
- Schätzen\
War in unserem Team schwierig, da alle wenig Erfahrungen mitgebracht haben.\
- Wichtigsten Features:\
Siehe Tasks im Codeberg Repo: Bäume suchen, anzeigen und hochladen.

2. Dokumentation
Siehe README.md in app/ webservice/


## Sonstiges
Es ist möglich sich einzuloggen und erst dadurch eine Übersicht über die eigenen Bäume zu erlangen. Jedoch muss dadurch umständlich die Seite MyTrees neu geladen werden. Aus diesem Grund haben wir uns entschlossen, den sessionUser hardzucoden. So ist es leichter alle Features zu testen. 
