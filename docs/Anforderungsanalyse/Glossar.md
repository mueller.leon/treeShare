# Glossar

**MyTrees** Übersichtsseite, auf der alle Bäume eingesehen werden können

**sessionUser** Variable, die den derzeitigen Nutzer speichert und die Session validiert

**Widget** Bezeichnet in Flutter Einheiten, aus denen die GUI zusammengesetzt ist

**Hot Reload** Ermöglicht, nach Codeänderungen die App zu aktualsieren, ohne sie neustarten zu müssen

**Bash** Eine Shell des Unix Betriebssystems
